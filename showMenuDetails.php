<?php
require('dbconnect.php');

session_start();
if (!isset($_SESSION['user_id'])) {
    header( 'Location: login.html' );
}
$user = $_SESSION['user_id'];
$q = htmlentities($_GET['q']);


$stmt = $mysqli->prepare("SELECT title, location, starttime, category FROM menu WHERE title =?");

if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}
 
$stmt->bind_param('s', $q);
 
$stmt->execute();
$stmt->bind_result($title, $price, $date);

echo '<FORM NAME="addcontent" ACTION="" METHOD="GET">';
while($stmt->fetch()){
    echo 'title: <INPUT TYPE="text" NAME="title" VALUE="'.htmlspecialchars($title).'">';
    echo '<br>price: <INPUT TYPE="text" NAME="price" VALUE="'.htmlspecialchars($price).'">';
    //echo '<br>Time offered: <INPUT TYPE="text" NAME="date" VALUE="'.htmlspecialchars($date).'">';
    if (htmlspecialchars($date)=="1"){
	echo '<br>time offered: <select name="date"><option value="1" selected="selected">sunday</option><option value="2">Monday</option></select><option value="3" selected="selected">tuesday</option><option value="4" selected="selected">wednesday</option><option value="5" selected="selected">thursday</option><option value="6" selected="selected">friday</option><option value="7" selected="selected">saturday</option>';
    }
    else if(htmlspecialchars($date)=="2"){
	echo '<br>time offered: <select name="date"><option value="2" selected="selected">monday</option><option value="1">sunday</option></select><option value="3" selected="selected">tuesday</option><option value="4" selected="selected">wednesday</option><option value="5" selected="selected">thursday</option><option value="6" selected="selected">friday</option><option value="7" selected="selected">saturday</option>';
    }
    else if(htmlspecialchars($date)=="3"){
	echo '<br>time offered: <select name="date"><option value="3" selected="selected">tuesday</option><option value="1">sunday</option></select><option value="2" selected="selected">monday</option><option value="4" selected="selected">wednesday</option><option value="5" selected="selected">thursday</option><option value="6" selected="selected">friday</option><option value="7" selected="selected">saturday</option>';
    }
    else if(htmlspecialchars($date)=="4"){
	echo '<br>time offered: <select name="date"><option value="4" selected="selected">wednesday</option><option value="1">sunday</option></select><option value="2" selected="selected">monday</option><option value="3" selected="selected">tuesday</option><option value="5" selected="selected">thursday</option><option value="6" selected="selected">friday</option><option value="7" selected="selected">saturday</option>';
    }
    else if(htmlspecialchars($date)=="5"){
	echo '<br>time offered: <select name="date"><option value="5" selected="selected">thursday</option><option value="1">sunday</option></select><option value="2" selected="selected">monday</option><option value="3" selected="selected">tuesday</option><option value="4" selected="selected">wednesday</option><option value="6" selected="selected">friday</option><option value="7" selected="selected">saturday</option>';
    }
    else if(htmlspecialchars($date)=="6"){
	echo '<br>time offered: <select name="date"><option value="6" selected="selected">friday</option><option value="1">sunday</option></select><option value="2" selected="selected">monday</option><option value="3" selected="selected">tuesday</option><option value="4" selected="selected">wednesday</option><option value="5" selected="selected">thursday</option><option value="7" selected="selected">saturday</option>';
    }
    else if(htmlspecialchars($date)=="7"){
	echo '<br>time offered: <select name="date"><option value="7" selected="selected">saturday</option><option value="1">sunday</option></select><option value="2" selected="selected">monday</option><option value="3" selected="selected">tuesday</option><option value="4" selected="selected">wednesday</option><option value="5" selected="selected">thursday</option><option value="6" selected="selected">friday</option>';
    }
    //echo '<br>category: <INPUT TYPE="text" NAME="category" VALUE="'.htmlspecialchars($category).'">';
    echo '<input type="hidden" name="eventid" value='.$q.'>';



}
echo '<br><INPUT TYPE="button" NAME="button" Value="Edit" onClick="editContent(this.form)" class="button"></FORM>';
 
$stmt->close();
 
?>
<?php
session_start();

require('dbconnect.php');

$username = $mysqli->real_escape_string($_POST['username']);
$email = $mysqli->real_escape_string($_POST['email']);
$hash = $mysqli->real_escape_string(crypt($_POST['password'], $_POST['password']));

$stmt = $mysqli->prepare("insert into users (username, email, password) values (?, ?, ?)");
if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}
 
$stmt->bind_param('sss', $username, $email, $hash);
 
$stmt->execute();
 
$stmt->close();

header( 'Location: login.html' );

exit;

?>

<?php
session_start();


echo '

<!DOCTYPE html>
<html><head><title>Calendar</title>

<script type="text/javascript">
function showEvents(date)
{

var xmlhttp=new XMLHttpRequest();
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
        document.getElementById(date+offset).innerHTML=this.responseText;
    }
  };
xmlhttp.open("GET","getevents.php?q="+(("0"+(month.toString())).slice(-2))+(("0"+(date.toString())).slice(-2))+year ,true);
xmlhttp.send(null);
}
</script>

<style type="text/css">
    /* Styling for the overall table */
table {
    font-family: "Avant Garde", Avantgarde, "Century Gothic", CenturyGothic, "AppleGothic", sans-serif;
    table-layout: fixed;
    border-collapse: collapse;
    width: 100%;
    }
/* Styling for the column headers (days of the week) */
th {
    padding: 0 0.5em;
    text-align: center;
    background-color:gray;
    color:white;
    }
/* Styling for the individual cells (days) */
td  {     
    font-size: medium;
    padding: 0.25em 0.25em;   
    width: 14%; 
    height: 80px;
    text-align: left;
    vertical-align: top;
    }
/* Styling for the date numbers */
.date  {
    font-family: "Avant Garde", Avantgarde, "Century Gothic", CenturyGothic, "AppleGothic", sans-serif;
    font-size: 85%;
    padding: 0.25em 0.25em;   
    text-align: left;
    vertical-align: top;
    }
/* Class for individual days (coming in future release) */
.sun {
     color:red;
     }
/* Hide the month element (coming in future release) */
th.month {
    visibility: hidden;
    display:none;
    }
.month {
    text-align: center;
    font-size: 40px;
    }
   
     .button {
        font-family: "Avant Garde", Avantgarde, "Century Gothic", CenturyGothic, "AppleGothic", sans-serif;
        -moz-box-shadow:inset 0px 1px 0px 0px #ffffff;
        -webkit-box-shadow:inset 0px 1px 0px 0px #ffffff;
        box-shadow:inset 0px 1px 0px 0px #ffffff;
        background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #ededed), color-stop(1, #dfdfdf));
        background:-moz-linear-gradient(top, #ededed 5%, #dfdfdf 100%);
        background:-webkit-linear-gradient(top, #ededed 5%, #dfdfdf 100%);
        background:-o-linear-gradient(top, #ededed 5%, #dfdfdf 100%);
        background:-ms-linear-gradient(top, #ededed 5%, #dfdfdf 100%);
        background:linear-gradient(to bottom, #ededed 5%, #dfdfdf 100%);
        filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=\'#ededed\', endColorstr=\'#dfdfdf\',GradientType=0);
        background-color:#ededed;
        -moz-border-radius:3px;
        -webkit-border-radius:3px;
        border-radius:3px;
        border:1px solid #dcdcdc; 
        display:inline-block;
        color:#777777;
        font-size:14px;
        font-weight:bold;
        padding:4px 16px;
        text-decoration:none;  
        text-shadow:0px 1px 0px #ffffff;
        
    }
    .button:hover { 
        background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #dfdfdf), color-stop(1, #ededed));
        background:-moz-linear-gradient(top, #dfdfdf 5%, #ededed 100%);
        background:-webkit-linear-gradient(top, #dfdfdf 5%, #ededed 100%);
        background:-o-linear-gradient(top, #dfdfdf 5%, #ededed 100%);
        background:-ms-linear-gradient(top, #dfdfdf 5%, #ededed 100%);
        background:linear-gradient(to bottom, #dfdfdf 5%, #ededed 100%);
        filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=\'#dfdfdf\', endColorstr=\'#ededed\',GradientType=0);
        background-color:#dfdfdf;
    }
    .button:active {
        position:relative;
        top:1px;
    }

}
    
</style>

</head>
<body>
<div id="login"></div>
<div id="overlay"></div>

<div id="specialBox">
</div>';


echo '<button id="prev" class="button" type="button">&lt;&lt;</button><button id="next" class="button" type="button">>></button><button id="logout" class="button" type="button">Logout</button>';
echo '<button id="addEventButton" class="button">Add event</button>';
echo '<div class="month" id="monthyear"> Menu </div>';

echo '<table border="1">
<tr><th><button id="prev" class="button" type="button">Sun</button></th><th><button id="prev" class="button" type="button">Mon</button></th><th><button id="prev" class="button" type="button">Tue</button></th><th><button id="prev" class="button" type="button">Wed</button></th><th><button id="prev" class="button" type="button">Thu</button></th><th><button id="prev" class="button" type="button">Fri</button></th><th><button id="prev" class="button" type="button">Sat</button></th></tr>';
//for ($i=0; $i<=5; $i++){
    echo '<tr>';
    for ($x=0; $x<=6; $x++){
     echo '<td><div class="date" id='.(($x)+1).'>&nbsp;</div></td>';
    }
    echo '</tr>';
//}
echo '</table>'
?>


<?php
require('dbconnect.php');

session_start();
if (!isset($_SESSION['user_id'])) {
    header( 'Location: login.html' );
}
$user = $_SESSION['user_id'];
$date = $_SESSION['date'];
$title = htmlentities($_GET['title']);
$price = htmlentities($_GET['price']);
$category = htmlentities($_GET['category']);
$comment = htmlentities($_GET['comment']);


$stmt = $mysqli->prepare("INSERT INTO menu SET id=?, title=?, price=?, date=?,comment=?");

if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}
 
$stmt->bind_param('isdis', $category, $title, $price, $date,$comment);
 
$stmt->execute();
 
$stmt->close();
 
?>
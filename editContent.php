
<?php
require('dbconnect.php');

session_start();
if (!isset($_SESSION['user_id'])) {
    die;
}
$user = $_SESSION['user_id'];
$id = htmlentities($_GET['id']);
$title = htmlentities($_GET['title']);
$price = htmlentities($_GET['price']);
$date = htmlentities($_GET['date']);
$stmt = $mysqli->prepare("UPDATE menu SET title=?, price=?, date=?, id=? where title =? ");

if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}
 
$stmt->bind_param('sdis', $title, $price, $date, $title);
 
$stmt->execute();
 
$stmt->close();
 
?>
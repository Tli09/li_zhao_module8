<?php session_start(); ?>
<!DOCTYPE html>
<html>
<head><title>Calendar Log Out Process</title></head>
<body>
	<?php
		session_destroy();
		header("Location: login.html");
		exit;
	?>
</body>
</html>
<?php
session_start();
require('dbconnect.php');

$username = $mysqli->real_escape_string($_POST['username']);
//$email = $mysqli->real_escape_string($_POST['email']);

$stmt = $mysqli->prepare("SELECT COUNT(*), user_id, password FROM users WHERE username=?");
if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}
 
$stmt->bind_param('s', $username);
$stmt->execute();

$stmt->bind_result($cnt, $user_id, $hash);
$stmt->fetch();

if( $cnt == 1 && crypt($_POST['password'], $_POST['password'])==$hash){
	$_SESSION['user_id'] = $user_id;
	$_SESSION['csrf_token'] = substr(md5(rand()), 0, 10);
	echo'true';
}else{
	echo'false';
}

exit;

?>